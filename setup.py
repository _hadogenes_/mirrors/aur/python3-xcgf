#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
  name='''XCGF''',
  version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(1637376063)),
  description='''Xyne's common generic functions, for internal use.''',
  author='''Xyne''',
  author_email='''gro xunilhcra enyx, backwards''',
  url='''http://xyne.dev/projects/python3-xcgf''',
  py_modules=['''XCGF'''],
)
